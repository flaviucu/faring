import http from 'http';
import express from 'express';
import cors from 'cors';
import morgan from 'morgan';
import bodyParser from 'body-parser';
import React from 'react';
import { renderToString } from 'react-dom/server';
import { ServerStyleSheet } from 'styled-components';

import initializeDb from './db';
import middleware from './middleware';
import api from './api';
import config from './config.json';
import { fetchCountries, } from './client/countries';
import App from './client/app';
import Html from './client/html';

let app = express();
app.server = http.createServer(app);

// logger
app.use(morgan('dev'));

// 3rd party middleware
app.use(cors({
	exposedHeaders: config.corsHeaders
}));

app.use(bodyParser.json({
	limit : config.bodyLimit
}));


app.get('/', (req, res) => {

	fetchCountries().then((countries) => {
		console.log('index', countries.length);
	  const sheet = new ServerStyleSheet();
	  const body = renderToString(sheet.collectStyles(<App />));
	  const styles = sheet.getStyleTags();
	  const title = 'Fares watchlist';

	  res.send(
		Html({
	      body,
	      styles,
	      title
	    })
	  );
	})
});


// api router
app.use('/api', api());

app.server.listen(process.env.PORT || config.port, () => {
	console.log(`Started on port ${app.server.address().port}`);
});

export default app;
