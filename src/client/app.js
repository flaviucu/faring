import React from 'react';
import styled from 'styled-components';

import { getCountries, } from './countries';




const AppContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  position: fixed;
  width: 100%;
  height: 100%;
  font-size: 40px;
`;

let selectedCountry = '';

function setCurrentCountry(e) {
	selectedCountry = e;
}

const App = () => 
	<AppContainer>
		<select>
			{getCountries().map(country => (
				<option
					value={country.alpha2Code.toLowerCase()}
					onChange={(e) => setCurrentCountry(e)}
				>{country.name}</option>
			))}
		</select>
		<button type="button">Search</button>
		{selectedCountry}
	</AppContainer>;

export default App;