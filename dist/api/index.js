'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _package = require('../../package.json');

var _express = require('express');

var _watchlist = require('./watchlist');

exports.default = function () {
	var api = (0, _express.Router)();

	api.get('/watchlist', function (req, res) {
		res.json((0, _watchlist.Watchlist)());
	});

	api.post('/watchlist', function (req, res) {
		(0, _watchlist.AddWatchlistFare)(req.body);
		res.json((0, _watchlist.Watchlist)());
	});

	api.delete('/watchlist/:id', function (req, res) {
		(0, _watchlist.DeleteWatchlistFare)(req.params.id);
		res.json((0, _watchlist.Watchlist)());
	});

	// perhaps expose some API metadata at the root
	api.get('/', function (req, res) {
		res.json({ version: _package.version });
	});

	return api;
}; // mount the facets resource
//# sourceMappingURL=index.js.map