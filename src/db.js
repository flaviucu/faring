// initial database values
let watchlist = [{
	fareId: 12345,
	fareContent: {
		content: 'test',
	}
}];

export function GetWatchlist () {
	return watchlist;
}

export function AddToWatchlist(newItem) {
	watchlist.push(newItem);
}

export function RemoveFromWatchlist(id) {
	watchlist = watchlist.filter((item) => item.fareId !== id);
}