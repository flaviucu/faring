import resource from 'resource-router-middleware';
import { GetWatchlist, AddToWatchlist, RemoveFromWatchlist, } from '../db';

export function Watchlist() {
	return GetWatchlist();
}

export function AddWatchlistFare(newFare) {
	AddToWatchlist(newFare);
}

export function DeleteWatchlistFare(deletedFareId) {
	RemoveFromWatchlist(deletedFareId);
}
