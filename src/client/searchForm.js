import React from 'react';
import styled from 'styled-components';

import Countries from './countries';




const AppContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  position: fixed;
  width: 100%;
  height: 100%;
  font-size: 40px;
`;

const countries = getCountries().then(() => {

	console.log(countries[0]);
});


const SearchForm = () => 
	<AppContainer>
		<select>
			{Countries.getCountries().map(country => (
				<option value={country.alpha2Code.toLowerCase()}>{country.name}</option>
			))}
		</select>
		<input type="text" placeholder="Select Destination" />
		<button type="button">Search</button>
	</AppContainer>;

export default SearchForm;