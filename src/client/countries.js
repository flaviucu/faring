import fetch from 'node-fetch';


let countries = [];

export function fetchCountries() {
	return fetch('https://restcountries.eu/rest/v2/all').then(function(response) {
		return response.json();
	}).then((res) => {
		countries = res;
		return countries;
	});
}

export function getCountries() {
	return countries;
}
