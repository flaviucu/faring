Getting Started
---------------

# Install dependencies
npm install

# Build
npm run build

# Start development live-reload server
PORT=8080 npm run dev

# Start production server:
PORT=8080 npm start


License
-------

MIT
