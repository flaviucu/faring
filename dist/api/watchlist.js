'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.Watchlist = Watchlist;
exports.AddWatchlistFare = AddWatchlistFare;
exports.DeleteWatchlistFare = DeleteWatchlistFare;

var _resourceRouterMiddleware = require('resource-router-middleware');

var _resourceRouterMiddleware2 = _interopRequireDefault(_resourceRouterMiddleware);

var _db = require('../db');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function Watchlist() {
	return (0, _db.GetWatchlist)();
}

function AddWatchlistFare(newFare) {
	(0, _db.AddToWatchlist)(newFare);
}

function DeleteWatchlistFare(deletedFareId) {
	(0, _db.RemoveFromWatchlist)(deletedFareId);
}
//# sourceMappingURL=watchlist.js.map