/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 5);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

module.exports = require("express");

/***/ }),
/* 1 */
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),
/* 2 */
/***/ (function(module, exports) {

module.exports = require("styled-components");

/***/ }),
/* 3 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["b"] = GetWatchlist;
/* harmony export (immutable) */ __webpack_exports__["a"] = AddToWatchlist;
/* harmony export (immutable) */ __webpack_exports__["c"] = RemoveFromWatchlist;
// initial database values
let watchlist = [{
	fareId: 12345,
	fareContent: {
		content: 'test'
	}
}];

function GetWatchlist() {
	return watchlist;
}

function AddToWatchlist(newItem) {
	watchlist.push(newItem);
}

function RemoveFromWatchlist(id) {
	watchlist = watchlist.filter(item => item.fareId !== id);
}

/***/ }),
/* 4 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["a"] = fetchCountries;
/* harmony export (immutable) */ __webpack_exports__["b"] = getCountries;
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_node_fetch__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_node_fetch___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_node_fetch__);


let countries = [];

function fetchCountries() {
	return __WEBPACK_IMPORTED_MODULE_0_node_fetch___default()('https://restcountries.eu/rest/v2/all').then(function (response) {
		return response.json();
	}).then(res => {
		countries = res;
		return countries;
	});
}

function getCountries() {
	return countries;
}

/***/ }),
/* 5 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_http__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_http___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_http__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_express__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_express___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_express__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_cors__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_cors___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_cors__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_morgan__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_morgan___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_morgan__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_body_parser__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_body_parser___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_body_parser__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_react__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_react_dom_server__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_react_dom_server___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_react_dom_server__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_styled_components__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_styled_components___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_styled_components__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__db__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__middleware__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__api__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__config_json__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__config_json___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_11__config_json__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__client_countries__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__client_app__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__client_html__ = __webpack_require__(19);

















let app = __WEBPACK_IMPORTED_MODULE_1_express___default()();
app.server = __WEBPACK_IMPORTED_MODULE_0_http___default.a.createServer(app);

// logger
app.use(__WEBPACK_IMPORTED_MODULE_3_morgan___default()('dev'));

// 3rd party middleware
app.use(__WEBPACK_IMPORTED_MODULE_2_cors___default()({
	exposedHeaders: __WEBPACK_IMPORTED_MODULE_11__config_json___default.a.corsHeaders
}));

app.use(__WEBPACK_IMPORTED_MODULE_4_body_parser___default.a.json({
	limit: __WEBPACK_IMPORTED_MODULE_11__config_json___default.a.bodyLimit
}));

app.get('/', (req, res) => {

	Object(__WEBPACK_IMPORTED_MODULE_12__client_countries__["a" /* fetchCountries */])().then(countries => {
		console.log('index', countries.length);
		const sheet = new __WEBPACK_IMPORTED_MODULE_7_styled_components__["ServerStyleSheet"]();
		const body = Object(__WEBPACK_IMPORTED_MODULE_6_react_dom_server__["renderToString"])(sheet.collectStyles(__WEBPACK_IMPORTED_MODULE_5_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_13__client_app__["a" /* default */], null)));
		const styles = sheet.getStyleTags();
		const title = 'Fares watchlist';

		res.send(Object(__WEBPACK_IMPORTED_MODULE_14__client_html__["a" /* default */])({
			body,
			styles,
			title
		}));
	});
});

// api router
app.use('/api', Object(__WEBPACK_IMPORTED_MODULE_10__api__["a" /* default */])());

app.server.listen(Object({"NODE_ENV":'production'}).PORT || __WEBPACK_IMPORTED_MODULE_11__config_json___default.a.port, () => {
	console.log(`Started on port ${app.server.address().port}`);
});

/* harmony default export */ __webpack_exports__["default"] = (app);

/***/ }),
/* 6 */
/***/ (function(module, exports) {

module.exports = require("http");

/***/ }),
/* 7 */
/***/ (function(module, exports) {

module.exports = require("cors");

/***/ }),
/* 8 */
/***/ (function(module, exports) {

module.exports = require("morgan");

/***/ }),
/* 9 */
/***/ (function(module, exports) {

module.exports = require("body-parser");

/***/ }),
/* 10 */
/***/ (function(module, exports) {

module.exports = require("react-dom/server");

/***/ }),
/* 11 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_express__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_express___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_express__);


/* unused harmony default export */ var _unused_webpack_default_export = (({ config, db }) => {
	let routes = Object(__WEBPACK_IMPORTED_MODULE_0_express__["Router"])();

	// add middleware here

	return routes;
});

/***/ }),
/* 12 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__package_json__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__package_json___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__package_json__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_express__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_express___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_express__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__watchlist__ = __webpack_require__(14);
// mount the facets resource




/* harmony default export */ __webpack_exports__["a"] = (() => {
	let api = Object(__WEBPACK_IMPORTED_MODULE_1_express__["Router"])();

	api.get('/watchlist', (req, res) => {
		res.json(Object(__WEBPACK_IMPORTED_MODULE_2__watchlist__["c" /* Watchlist */])());
	});

	api.post('/watchlist', (req, res) => {
		Object(__WEBPACK_IMPORTED_MODULE_2__watchlist__["a" /* AddWatchlistFare */])(req.body);
		res.json(Object(__WEBPACK_IMPORTED_MODULE_2__watchlist__["c" /* Watchlist */])());
	});

	api.delete('/watchlist/:id', (req, res) => {
		Object(__WEBPACK_IMPORTED_MODULE_2__watchlist__["b" /* DeleteWatchlistFare */])(req.params.id);
		res.json(Object(__WEBPACK_IMPORTED_MODULE_2__watchlist__["c" /* Watchlist */])());
	});

	// perhaps expose some API metadata at the root
	api.get('/', (req, res) => {
		res.json({ version: __WEBPACK_IMPORTED_MODULE_0__package_json__["version"] });
	});

	return api;
});

/***/ }),
/* 13 */
/***/ (function(module, exports) {

module.exports = {"name":"express-es6-rest-api","version":"0.0.1","description":"Starter project for an ES6 RESTful Express API","main":"dist","scripts":{"dev":"nodemon -w src --exec \"babel-node src --presets es2015,stage-0\"","build":"webpack","start":"node dist","prestart":"npm run -s build","test":"eslint src"},"eslintConfig":{"extends":"eslint:recommended","parserOptions":{"ecmaVersion":7,"sourceType":"module"},"env":{"node":true},"rules":{"no-console":0,"no-unused-vars":1}},"repository":"developit/express-es6-rest-api","author":"Jason Miller <jason@developit.ca>","license":"MIT","dependencies":{"babel-preset-react":"^6.24.1","body-parser":"^1.13.3","compression":"^1.5.2","cors":"^2.7.1","express":"^4.13.3","morgan":"^1.8.0","node-fetch":"^2.2.0","react":"^16.2.0","react-dom":"^16.2.0","resource-router-middleware":"^0.6.0","styled-components":"^2.2.4"},"devDependencies":{"babel-cli":"^6.9.0","babel-core":"^6.9.0","babel-preset-es2015":"^6.9.0","babel-preset-stage-0":"^6.5.0","eslint":"^3.1.1","nodemon":"^1.9.2","babel-loader":"^7.1.2","babel-preset-env":"^1.6.1","babel-preset-react":"^6.11.1","webpack":"^3.8.1","webpack-node-externals":"^1.2.0"}}

/***/ }),
/* 14 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["c"] = Watchlist;
/* harmony export (immutable) */ __webpack_exports__["a"] = AddWatchlistFare;
/* harmony export (immutable) */ __webpack_exports__["b"] = DeleteWatchlistFare;
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_resource_router_middleware__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_resource_router_middleware___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_resource_router_middleware__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__db__ = __webpack_require__(3);



function Watchlist() {
	return Object(__WEBPACK_IMPORTED_MODULE_1__db__["b" /* GetWatchlist */])();
}

function AddWatchlistFare(newFare) {
	Object(__WEBPACK_IMPORTED_MODULE_1__db__["a" /* AddToWatchlist */])(newFare);
}

function DeleteWatchlistFare(deletedFareId) {
	Object(__WEBPACK_IMPORTED_MODULE_1__db__["c" /* RemoveFromWatchlist */])(deletedFareId);
}

/***/ }),
/* 15 */
/***/ (function(module, exports) {

module.exports = require("resource-router-middleware");

/***/ }),
/* 16 */
/***/ (function(module, exports) {

module.exports = {"port":8080,"bodyLimit":"100kb","corsHeaders":["Link"]}

/***/ }),
/* 17 */
/***/ (function(module, exports) {

module.exports = require("node-fetch");

/***/ }),
/* 18 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_styled_components__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_styled_components___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_styled_components__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__countries__ = __webpack_require__(4);





const AppContainer = __WEBPACK_IMPORTED_MODULE_1_styled_components___default.a.div`
  display: flex;
  align-items: center;
  justify-content: center;
  position: fixed;
  width: 100%;
  height: 100%;
  font-size: 40px;
`;

let selectedCountry = '';

function setCurrentCountry(e) {
	selectedCountry = e;
}

const App = () => __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
	AppContainer,
	null,
	__WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
		'select',
		null,
		Object(__WEBPACK_IMPORTED_MODULE_2__countries__["b" /* getCountries */])().map(country => __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
			'option',
			{
				value: country.alpha2Code.toLowerCase(),
				onChange: e => setCurrentCountry(e)
			},
			country.name
		))
	),
	__WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(
		'button',
		{ type: 'button' },
		'Search'
	),
	selectedCountry
);

/* harmony default export */ __webpack_exports__["a"] = (App);

/***/ }),
/* 19 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/**
 * Html
 * This Html.js file acts as a template that we insert all our generated
 * application strings into before sending it to the client.
 */
const Html = ({ body, styles, title }) => `
  <!DOCTYPE html>
  <html>
    <head>
      <title>${title}</title>
      ${styles}
    </head>
    <body style="margin:0">
      <div id="app">${body}</div>
    </body>
  </html>
`;

/* harmony default export */ __webpack_exports__["a"] = (Html);

/***/ })
/******/ ]);