	// mount the facets resource
import { version } from '../../package.json';
import { Router } from 'express';
import { Watchlist, AddWatchlistFare, DeleteWatchlistFare, } from './watchlist';

export default () => {
	let api = Router();

	api.get('/watchlist', (req, res) => {
		res.json(Watchlist());
	});

	api.post('/watchlist', (req, res) => {
		AddWatchlistFare(req.body);
		res.json(Watchlist());
	});

	api.delete('/watchlist/:id', (req, res) => {
		DeleteWatchlistFare(req.params.id);
		res.json(Watchlist());
	});

	// perhaps expose some API metadata at the root
	api.get('/', (req, res) => {
		res.json({ version });
	});

	return api;
}
